<?php

declare(strict_types=1);

namespace Altek\Accountant\Tests\Database\Factories;

use Altek\Accountant\Tests\Models\Article;
use Illuminate\Database\Eloquent\Factories\Factory;

class ArticleFactory extends Factory
{
    /**
     * {@inheritDoc}
     */
    protected $model = Article::class;

    /**
     * {@inheritDoc}
     */
    public function definition(): array
    {
        return [
            'title'        => $this->faker->unique()->sentence,
            'content'      => $this->faker->unique()->paragraph(6),
            'published_at' => null,
            'reviewed'     => $this->faker->randomElement([0, 1]),
        ];
    }
}
